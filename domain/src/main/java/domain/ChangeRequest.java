package domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

@XmlRootElement(name = "changeRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChangeRequest implements Serializable {
	private static final long serialVersionUID = -9182076387821713741L;

	@XmlElement
	@XmlSchemaType(name = "boolean")
	Boolean approved = false;
	
	@XmlElement
	@XmlSchemaType(name = "date")
	Date date;
	
	@XmlElement
	@XmlSchemaType(name = "string")
	String system;
	
	@XmlElement
	@XmlSchemaType(name = "string")
	String purpose;

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

}
