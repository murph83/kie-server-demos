package client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.remote.client.api.RemoteRestRuntimeEngineFactory;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;

import domain.ChangeRequest;

public class ProcessClient {

	private static final String URL = "http://172.17.0.2:8080/business-central";
	private static final String USER = "bpmsAdmin";
	private static final String PASSWORD = "jbossadmin1!";
	private static final String DEPLOYMENT_ID = "example:approvals-process:1.0";
	private static final String PROCESS_ID = "approval-process";
	private static final String TEXT = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibu";

	public static void main(String[] args) throws MalformedURLException {
		// configure client
		URL url = new URL(URL);
		RuntimeEngine runtimeEngine = configure(url, USER, PASSWORD,
				DEPLOYMENT_ID);
		KieSession kSession = runtimeEngine.getKieSession();

		// generate commands
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("changeRequest", getChangeRequest());
		

		// send the command via REST API
		ProcessInstance process = kSession.startProcess(PROCESS_ID, parameters);

		// process results
		

	}

	public static RuntimeEngine configure(URL url, String userName,
			String password, String deploymentId) {
		RemoteRestRuntimeEngineFactory factory = RemoteRuntimeEngineFactory
				.newRestBuilder().addUrl(url).addUserName(userName)
				.addPassword(password).addDeploymentId(deploymentId)
				.buildFactory();

		return factory.newRuntimeEngine();
	}

	private static ChangeRequest getChangeRequest() {
		Calendar crDate = Calendar.getInstance();
		// crDate.add(Calendar.WEEK_OF_YEAR, 1);

		ChangeRequest cr = new ChangeRequest();
		cr.setPurpose(TEXT);
		cr.setDate(crDate.getTime());
		return cr;
	}

}
