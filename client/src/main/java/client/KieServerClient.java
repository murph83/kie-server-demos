package client;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.runtime.BatchExecutionCommandImpl;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.runtime.ExecutionResults;
import org.kie.internal.command.CommandFactory;
import org.kie.internal.runtime.helper.BatchExecutionHelper;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;

import domain.ChangeRequest;

public class KieServerClient {

	public static final String USERNAME = "bpmsAdmin";
	public static final String PASSWORD = "jbossadmin1!";
	public static final String SERVER_URL = "http://172.17.0.2:8080/kie-server/services/rest/server";
	public static final String CONTAINER = "approvals";
	public static final String TEXT = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibu";

	public static void main(String args[]) {

		// configure client
		KieServicesClient client = configure(SERVER_URL, USERNAME, PASSWORD);

		// generate commands
		List<GenericCommand<?>> commands = new ArrayList<GenericCommand<?>>();
		commands.add((GenericCommand<?>) CommandFactory.newStartProcess("approval-process"));
		commands.add((GenericCommand<?>) CommandFactory.newInsert(getChangeRequest(true), "pass"));
		commands.add((GenericCommand<?>) CommandFactory.newInsert(getChangeRequest(false), "fail"));
		commands.add((GenericCommand<?>) CommandFactory.newFireAllRules("fire-identifier"));
		BatchExecutionCommand command = new BatchExecutionCommandImpl(commands);

		// marshal object to xml
		String xStreamXml = BatchExecutionHelper.newXStreamMarshaller().toXML(
				command);

		// send the xml command via REST API to the Decision Server
		ServiceResponse<String> response = client.executeCommands(CONTAINER,
				xStreamXml);
		System.out.println(response.getResult());

		// process results
		ExecutionResults results = ((ExecutionResults) BatchExecutionHelper
				.newXStreamMarshaller().fromXML(response.getResult()));

	}

	public static KieServicesClient configure(String url, String username,
			String password) {

		KieServicesConfiguration config = KieServicesFactory
				.newRestConfiguration(url, username, password);
		return KieServicesFactory.newKieServicesClient(config);

	}

	private static ChangeRequest getChangeRequest(boolean valid) {
		Calendar crDate = Calendar.getInstance();
		if (valid) {
			crDate.add(Calendar.WEEK_OF_YEAR, 1);
		}

		ChangeRequest cr = new ChangeRequest();
		cr.setPurpose(TEXT);
		cr.setDate(crDate.getTime());
		return cr;
	}

}
